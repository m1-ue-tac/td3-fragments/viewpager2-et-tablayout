package com.univlille.bidonviewpagertablayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager2 viewPager2 = (ViewPager2) findViewById(R.id.viewPager2);
        viewPager2.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return  BlankFragment.newInstance("je suis le fragement ", Integer.toString(position+1));
            }

            @Override
            public int getItemCount() {
                return 10;
            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //new TabLayoutMediator(tabLayout, viewPager2, (tab, position) -> tab.setText("OBJECT " + (position + 1))).attach();
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText("OBJECT " + (position + 1));
            }
        });
        tabLayoutMediator.attach();

    }
}